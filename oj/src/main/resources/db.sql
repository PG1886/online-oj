-- create database
drop database if exists oj;
create database oj;
use oj;

-- create tables and insert data
drop table if exists userInfo;                        -- 用户表
create table userInfo(
    id int primary key auto_increment,
    username varchar(50) unique,
    nickname varchar(50) default 'user111',
    password varchar(50) not null,
    email varchar(50) not null,
    ip varchar(20)
);

drop table if exists oj;
create table oj(
    id int primary key auto_increment,
    title varchar(50),
    level varchar(50),             -- 难度
    description varchar(4096),     -- 要求
    templateCode varchar(4096),    -- 代码提示
    testCode varchar(4096)         -- 测试案例
);

insert into oj(title,level,description,templateCode,templateCode)
value(
    '合并区间',
    'easy',

)
