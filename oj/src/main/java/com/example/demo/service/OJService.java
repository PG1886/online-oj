package com.example.demo.service;

import com.example.demo.common.Task;
import com.example.demo.mapper.OJMapper;
import com.example.demo.model.Answer;
import com.example.demo.model.OJ;
import com.example.demo.model.Question;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 题目相关业务服务层
 */
@Service
public class OJService {

    @Autowired
    private OJMapper ojMapper;

    /**
     * 查询数据库先返回100条数据  TODO：后续添加分页
     * @return
     */
    public List<OJ> getProblemList() {
        return ojMapper.queryAll(0,100);
    }

    /**
     * 查询数据库 根据id查询题目详情
     * @param id
     * @return
     */
    public OJ getProblem(Integer id) {
        return ojMapper.queryById(id);
    }

    /**
     * 处理用户提交的代码
     * @param question
     * @return  reason:编译出错的原因  stderr：运行时异常原因 stdout：测试结果
     */
    public Answer compile(Question question) {
        // 1.将用户提交的代码与数据库中测试用例进行拼接
        // 查询数据库测试代码
        String testCode = ojMapper.queryById(question.getId()).getTestCode();
        // 截取最后一个}
        int index = question.getCode().lastIndexOf("}");
        if (index == -1) {
            // 说明代码不完整
            return null;
        }
        StringBuilder result = new StringBuilder(question.getCode().substring(0, index));
        // 拼接测试代码
        result.append("\t" + testCode);
        result.append("\n}\n");
        // 拼接 }
        question.setCode(result.toString());

        // 2.创建Task实例调用compileAndRun开始执行
        Task task = new Task();
        Answer answer = task.compileAndRun(question);

        // 3.返回结果
        return answer;
    }
}
