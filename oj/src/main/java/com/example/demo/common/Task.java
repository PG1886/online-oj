package com.example.demo.common;

import com.example.demo.component.Scheduled;
import com.example.demo.model.Answer;
import com.example.demo.model.Question;
import org.springframework.beans.factory.annotation.Autowired;

import java.io.*;
import java.util.UUID;

/**
 * 每次 ”编译+运行“  的一个过程就是一个Task
 */
public class Task {

    /**
     * 这些临时文件 服务器进程获取子进程编译运行代码的结果，也就是进程之间通信
     * question:多个不同请求同时请求时该文件相互覆盖
     * 解决：1.加锁（性能极低） 2.让每个请求生成不同的目录（资源消耗大） 3.加锁（类似哈希桶加锁，针对不同目录加速）+多个不同的目录+轮询 4.方法2+定期删除（推荐）
     */
    // 所有临时文件的目录
    private String WORD_DIR = null;
    // 所有代码的类名
    private String CLASS = null;
    // 用户上传代保存码文件
    private String CODE = null;
    // 用户上传代码进程的编译时标准错误文件
    private String COMPILE_ERROR = null;
    // 用户上传代码进程的标准输出文件
    private String STDOUT = null;
    // 用户上传代码进程的运行时标准错误文件
    private String STDERR = null;

    public Task() {
        WORD_DIR = "./tmp/" + UUID.randomUUID().toString() + "/";
        CLASS = "Main";
        CODE = WORD_DIR + CLASS + ".java";
        COMPILE_ERROR = WORD_DIR + "compileError.txt";
        STDOUT = WORD_DIR + "stdout.txt";
        STDERR = WORD_DIR + "stderr.txt";
    }

    /**
     * 编译运行代码
     * @param question
     * @return
     */
    public Answer compileAndRun(Question question)  {
        // 创建线程
        Answer answer = new Answer();

        // 0.创建临时目录
        File workDir = new File(WORD_DIR);
        if (!workDir.exists()) {
            // 目录不存在，创建目录
            workDir.mkdirs();
        }

        // 添加代码非法校验
        if (!BlackCode.checkCode(question.getCode())) {
            System.out.println("用户提交非法代码");
            answer.setError(3);
            answer.setStderr("服务器以记录您的非法行为，请立即停止！！");
            return answer;
        }

        // 1.将question里的code（用户提交的代码）写入java文件  ：类名与文件名必须相同，此处规定为Main.java
        FileUtil.writeFile(CODE,question.getCode());

        // 2.创建子进程，调用javac命令编译   如果编译出错就会写入标准错误文件
        String compileCmd = String.format("javac -encoding utf8 %s -d %s",CODE,WORD_DIR);
        System.out.println("编译命令生成：" + compileCmd);
        ProcessUtil.run(compileCmd,null,COMPILE_ERROR); // 开始编译

            // 读取编译错误文件：如果为空则编译正确，如果有内容则编译有错误
        String compileError = FileUtil.readFile(COMPILE_ERROR);
        if (!compileError.equals("")) {
            // 编译错误,构造错误信息返回
            System.out.println("编译出错：" + compileError);
            answer.setError(1);
            answer.setReason(compileError);
            return answer;
        }

        // 3.创建子进程，调用java命令执行    会把标准输入与标准输出获取到
        String runCmd = String.format("java -classpath %s %s",WORD_DIR,CLASS);
        System.out.println("运行命令生成：" + runCmd);
        ProcessUtil.run(runCmd,STDOUT,STDERR);

            // 读取运行时标准错误文件, 正常情况用户不可能存在标准错误。如果该文件空则正常，如果不为空则存在异常
        String runError = FileUtil.readFile(STDERR);
        if (!runError.equals("")) {
            // 运行时出错，存在异常
            System.out.println("运行出错：" + runError);
            answer.setError(2);
            answer.setStderr(runError);
            return answer;
        }

        // 4.父进程获取编译结果，打包为Answer对象进行返回
        String runOut = FileUtil.readFile(STDOUT);
        answer.setError(0);
        answer.setStdout(runOut);

        return answer;
    }


    /**
     * 测试
     * @param args
     */
    public static void main(String[] args) {
        System.out.println(System.getProperty("user.dir"));
    }
}
