package com.example.demo.common;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

/**
 * 执行对应的java进程，将执行结果写入文件
 */
public class ProcessUtil {
    /**
     * 将创建进程执行相应的代码进程封装
     * @param cmd  指令
     * @param stdoutFile 标准输入的文件复制路径
     * @param stderrFile 标准错误的文件复制路径
     * @return 进程执行的状态码
     */
    public static int run(String cmd, String stdoutFile, String stderrFile) {
        try {
            /* 1 通过Runtime创建实例，，执行exec方法 */
            Process process = Runtime.getRuntime().exec(cmd);

            /* 2 获取标准输出，写入指定文件 */
            if (stdoutFile != null) {
                // 读取标准输入，写入文件
                InputStream stdoutFrom = process.getInputStream();
                FileOutputStream stdoutTo = new FileOutputStream(stdoutFile);
                while (true) {
                    int ch = stdoutFrom.read();
                    if (ch == -1) {
                        break;
                    }

                    stdoutTo.write(ch);
                }

                // 释放资源
                stdoutFrom.close();
                stdoutTo.close();
            }

            /* 3 获取标准错误，写入指定文件 */
            if (stderrFile != null) {
                // 读取标准错误
                InputStream stderrFrom = process.getErrorStream();
                FileOutputStream stderrTo = new FileOutputStream(stderrFile);
                while (true) {
                    int ch = stderrFrom.read();
                    if (ch == -1) {
                        break;
                    }

                    stderrTo.write(ch);
                }

                // 释放资源
                stderrFrom.close();
                stderrTo.close();
            }

            /* 4 等待子进程结束，拿到状态码返回 */
            int exitCode = process.waitFor();
            return exitCode;
        } catch (IOException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        return 1;
    }
}
