package com.example.demo.common;

import com.example.demo.component.Scheduled;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

/**
 * 文件读写封装
 */
public class FileUtil {
    /**
     * 读取文件内容到String中
     * @param path
     * @return
     */
    public static String readFile(String path) {
        // StringBuilder相比String来说更高效，String它追加的底层是StringBuilder.append.toString
        StringBuilder result = new StringBuilder();

        // 字符流读取
        try (FileReader fileReader = new FileReader(path)) {
            while (true) {
                int ch = fileReader.read();
                if (ch == -1) {
                    break;
                }

                result.append((char) ch);
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return result.toString();
    }

    /**
     * 将内容写入对应文件
     * @param path
     * @param content
     */
    public static void writeFile(String path,String content) {
        try(FileWriter fileWriter = new FileWriter(path)) {
            fileWriter.write(content);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
