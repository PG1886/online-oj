package com.example.demo.common;

import lombok.extern.slf4j.Slf4j;

import java.util.List;

/**
 * IP黑名单:通过拦截器实现,在访问接口之前获取IP如果不在黑名单则继续访问，如果在黑名单则进行拦截
 *
 * 规则：上传非法代码2次 + 上传非法请求2次
 */
@Slf4j
public class BlackIP {
    // IP黑名单，后续持久化
    private static List<String> blackIP;

    /**
     *  添加
     * @param IP
     */
    public static void add(String IP) {
         blackIP.add(IP);
         log.trace("已禁止当前IP" + IP);
    }

    /**
     * 删除
     * @param IP
     */
    public static void del(String IP) {
        blackIP.remove(IP);
        log.trace("已移除当前IP" + IP);
    }

    /**
     * 是否在黑名单
     * @param IP
     * @return
     */
    public static boolean isBlack(String IP) {
       return blackIP.contains(IP);
    }
}
