package com.example.demo.common;

import java.util.ArrayList;
import java.util.List;

public class BlackCode {
    // 非法关键字
    private static List<String> blackCodes = new ArrayList<>();

    // 非法关键字
    static {
        blackCodes.add("Runtime");
        blackCodes.add("exec");
        blackCodes.add("java.io");
        blackCodes.add("java.net");
        blackCodes.add("Thread");
        blackCodes.add("Process");
        blackCodes.add("Scanner");
    }

    /**
     * 查找代码中是否有非法代码
     * @param code
     * @return
     */
    public static boolean checkCode(String code) {
        for (String target : blackCodes) {
            // 查找词汇
            int pos = code.indexOf(target);
            if (pos >= 0) {
                return false;
            }
        }
        return true;
    }
}
