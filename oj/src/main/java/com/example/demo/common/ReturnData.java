package com.example.demo.common;

import lombok.Data;

/**
 * 返回前端的数据统一格式
 */
@Data
public class ReturnData {
    private int code;
    private String message;
    private Object data;

    private ReturnData(int code,String message,Object data) {
        this.code = code;
        this.message = message;
        this.data = data;
    }

    public static ReturnData success(Object data) {
        return new ReturnData(200,null,data);
    }

    public static ReturnData success(int code,Object data) {
        return new ReturnData(code,"",data);
    }

    public static ReturnData success(int code,String message,Object data) {
        return new ReturnData(code,message,data);
    }

    public static ReturnData fail(int code,String message) {
        return new ReturnData(code,message,null);
    }

    public static ReturnData fail(int code,String message,Object data) {
        return new ReturnData(code,message,data);
    }
}
