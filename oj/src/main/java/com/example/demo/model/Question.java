package com.example.demo.model;

import lombok.Data;

/**
 * 表示task的输入内容：用户提交的代码
 */
@Data
public class Question {
    private Integer id;
    private String code;  // 代码
}
