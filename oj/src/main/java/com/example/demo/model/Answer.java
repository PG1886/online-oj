package com.example.demo.model;

import lombok.Data;

/**
 * 一个task的执行结果
 */
@Data
public class Answer {
    private int error;      // 0--ok  1--error 2--throw
    private String reason;
    private String stdout;  // 标准输入
    private String stderr;  // 标准错误
}
