package com.example.demo.model;

import lombok.Data;

/**
 * 与数据库中oj表字段相同
 */
@Data
public class OJ {
    private int id;
    private String title;
    private String level;
    private String description;
    private String templateCode;
    private String testCode;
}
