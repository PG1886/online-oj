package com.example.demo.controller;

import com.example.demo.common.ReturnData;
import com.example.demo.model.Answer;
import com.example.demo.model.OJ;
import com.example.demo.model.Question;
import com.example.demo.service.OJService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;

@Controller
@RequestMapping("/problems")
@ResponseBody
public class OJController {

    @Autowired
    private OJService ojService;

    /**
     * 获取题目列表
     * @return
     */
    @GetMapping("/list")
    public ReturnData getProblemList() {
        return ReturnData.success(200,ojService.getProblemList());
    }

    /**
     * 获取题目详情
     * @param id
     * @return
     */
    @GetMapping("/problem")
    public ReturnData getProblem(@RequestParam("id") Integer id) {
        // 非法校验
        if (id == null || id <= 0) {
            return ReturnData.fail(500,"非法参数");
        }

        // 交给service进行查询业务
        OJ problem = ojService.getProblem(id);
        if (problem == null) {
            // 查询不到
            return ReturnData.fail(500,"非法参数异常");
        }

        // 返回数据
        return ReturnData.success(200,problem);
    }

    /**
     * 获取用户提交代码进行编译运行
     * @return reason:编译出错的原因  stderr：运行时异常原因 stdout：测试结果  error 0--ok  1--error 2--throw
     */
    @PostMapping("/compile")
    public ReturnData compileAndRun(Question question) {
        System.out.println(question);
        // 参数非法校验
        if (question == null
                || question.getCode() == ""
                || question.getCode() == null
                || question.getId() == null
                || question.getId() <= 0) {
            return ReturnData.fail(5000,"非法参数");
        }

        if (!StringUtils.hasLength(question.getCode())) {
            return ReturnData.fail(500,"非法代码");
        }

        // 交给service层进行编译运行
        Answer answer = ojService.compile(question);
        if (answer == null) {
            // 用户提交代码非法
            return ReturnData.fail(500,"非法代码");
        }
        // 返回结果
        return ReturnData.success(200,"",answer);
    }
}
