package com.example.demo.mapper;

import com.example.demo.model.OJ;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 *
 * 题目表映射类
 * 数据库表相关的操作：1.新增题目  2.删除题目  3.查询题目列表  4.查询题目详情
 */
@Mapper
public interface OJMapper {
    /**
     * 1.新增题目
     * @param oj
     * @return
     */
    int addProblem(OJ oj);

    /**
     * 2.删除题目
     * @param id
     * @return
     */
    int delProblem(@Param("id") Integer id);

    /**
     * 3.查询题目列表:分页查询
     * @return
     */
    List<OJ> queryAll(@Param("from") Integer from,@Param("size") Integer size);

    /**
     * 4.查询题目详情
     * @param id
     * @return
     */
    OJ queryById(@Param("id") Integer id);
}
