package com.example.demo;

import com.example.demo.mapper.OJMapper;
import com.example.demo.model.OJ;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class OjApplication {
// 1/比赛模块、 2.期末考试练习  3.IP黑名单

	public static void main(String[] args) {
		SpringApplication.run(OjApplication.class, args);
	}
}
