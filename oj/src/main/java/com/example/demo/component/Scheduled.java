package com.example.demo.component;

import org.aspectj.lang.annotation.After;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.stereotype.Component;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.attribute.BasicFileAttributes;
import java.nio.file.attribute.FileTime;
import java.time.Duration;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Arrays;

/**
 * 定时删除临时文件
 */
@Aspect
@Component
public class Scheduled {

    public static final Object locker = new Object();

    /**
     * 启动线程扫描
     */
    public Scheduled() {
        new Thread() {
            @Override
            public void run() {
                while (true) {
                    synchronized (locker) {
                        try {
                            System.out.println(Thread.currentThread().getId() + "正在执行");
                            long time = del();
                            System.out.println("执行文件删除返回" + time);
                            if (time != -1) {
                                System.out.println("未删除线程阻塞" + time);
                                locker.wait(time);
                            }
                        } catch (IOException e) {
                            e.printStackTrace();
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                    }
                }
            }
        }.start();
    }

    /**
     * 文件是否过期
     * @param item
     * @return
     * @throws IOException
     */
    private long isAfter(File item) throws IOException {
        // 获取创建时间
        FileTime time = Files.readAttributes(Paths.get(item.toString()), BasicFileAttributes.class).creationTime();
        LocalDateTime create = LocalDateTime.ofInstant(time.toInstant(), ZoneId.systemDefault());
        // 获得过期时间对应的日期时间
        LocalDateTime deadline = create.plusMinutes(1);
        // 计算所剩过期时间
        Duration duration = Duration.between(create,deadline);

        // 转为long类型
        long millis = duration.toMillis();
        System.out.println("文件还剩" + millis + "s过期");
        return LocalDateTime.now().isAfter(deadline) ? -1 : millis;
    }

    /**
     * 删除操作
     * @return
     * @throws IOException
     */
    public long del() throws IOException, InterruptedException {
        // 创建File对象获取目标目录的子目录
        File dir = new File(System.getProperty("user.dir") + "\\tmp");
        File[] files = dir.listFiles();

        // 记录所有子目录的所剩过期时间
        long millis[] = new long[files.length == 0 ? 10 : files.length];
        int i = 0;

        // 遍历目录
        for (File item:files) {
            millis[i] = isAfter(item);
            if (millis[i++] == -1) {
                // 删除
                System.out.println("删除：" + item);
                for(File del : item.listFiles()) {
                    del.delete();
                }
                return item.delete() ? -1 : -2;
            }
        }

        // 排序找到最先要删除文件的所剩时间
        Arrays.sort(millis);
        synchronized (locker) {
            if (millis[0] <= 0) {
                // 说明目录中没有文件，等待新的文件创建时唤醒线程
                System.out.println("没有文件待删除进入阻塞");
                locker.wait();
                return 1;
            }
            return millis[0];
        }
    }

    // 定义切点
    @Pointcut("execution(* com.example.demo.controller.OJController.compileAndRun(..))")
    public void  point(){}

    // 定义通过
    @After("point()")
    public void after() {
        // 此时有新的文件创建，唤醒线程
        synchronized (locker) {
            System.out.println("新的代码执行完毕，唤醒线程");
            locker.notify();
        }
    }
}
