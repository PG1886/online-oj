package com.example.demo.interceptor;

import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;

@ControllerAdvice
public class ExceptionInterceptor {
    @ExceptionHandler(Exception.class)
    @ResponseBody
    public Object exception(Exception e) {
        return e.getMessage();
    }
}
