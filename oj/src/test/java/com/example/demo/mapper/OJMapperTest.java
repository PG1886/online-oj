package com.example.demo.mapper;

import com.example.demo.model.OJ;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.transaction.annotation.Transactional;


@SpringBootTest
class OJMapperTest {

    @Autowired
    private OJMapper ojMapper;

    @Test
    void addProblem() {
        OJ oj = new OJ();
        oj.setId(1);
        oj.setTitle("合并区间");
        oj.setDescription("描述\n" +
                "给出一组区间，请合并所有重叠的区间。\n" +
                "请保证合并后的区间按区间起点升序排列。\n" +
                "\n" +
                "数据范围：区间组数 \n" +
                "0\n" +
                "≤\n" +
                "�\n" +
                "≤\n" +
                "2\n" +
                "×\n" +
                "1\n" +
                "0\n" +
                "5\n" +
                "0≤n≤2×10 \n" +
                "5\n" +
                " ，区间内 的值都满足 \n" +
                "0\n" +
                "≤\n" +
                "�\n" +
                "�\n" +
                "�\n" +
                "≤\n" +
                "2\n" +
                "×\n" +
                "1\n" +
                "0\n" +
                "5\n" +
                "0≤val≤2×10 \n" +
                "5\n" +
                " \n" +
                "要求：空间复杂度 \n" +
                "�\n" +
                "(\n" +
                "�\n" +
                ")\n" +
                "O(n)，时间复杂度 \n" +
                "�\n" +
                "(\n" +
                "�\n" +
                "�\n" +
                "�\n" +
                "�\n" +
                "�\n" +
                ")\n" +
                "O(nlogn)\n" +
                "进阶：空间复杂度 \n" +
                "�\n" +
                "(\n" +
                "�\n" +
                "�\n" +
                "�\n" +
                ")\n" +
                "O(val)，时间复杂度\n" +
                "�\n" +
                "(\n" +
                "�\n" +
                "�\n" +
                "�\n" +
                ")\n" +
                "O(val)\n" +
                "示例1\n" +
                "输入：\n" +
                "[[10,30],[20,60],[80,100],[150,180]]\n" +
                "复制\n" +
                "返回值：\n" +
                "[[10,60],[80,100],[150,180]]\n" +
                "复制\n" +
                "示例2\n" +
                "输入：\n" +
                "[[0,10],[10,20]]\n" +
                "复制\n" +
                "返回值：\n" +
                "[[0,20]]");
        oj.setLevel("easy++");
        oj.setTemplateCode("import java.util.ArrayList;\n" +
                "\n" +
                "class Interval {\n" +
                "    public int start;\n" +
                "    public int end;\n" +
                "    public Interval() { start = 0; end = 0; }\n" +
                "    public Interval(int s, int e) { start = s; end = e; }\n" +
                "\n" +
                "    @Override\n" +
                "    public String toString() {\n" +
                "        return \"[\" + start +\n" +
                "                \", \" + end +\n" +
                "                ']';\n" +
                "    }\n" +
                "}\n" +
                "\n" +
                "public class Main {\n" +
                "    public ArrayList<Interval> merge(ArrayList<Interval> intervals) {\n" +
                "        \n" +
                "    }\n" +
                "}\n");
        oj.setTestCode("public static String isTrue(ArrayList<Interval> test,ArrayList<Interval> success) {\n" +
                "        if (success == null && test != null || test == null && success != null) {\n" +
                "            return \"实际输出：\" + test + \"\\n\";\n" +
                "        }\n" +
                "        if (success == null && test == null) {\n" +
                "            return \"success\";\n" +
                "        }\n" +
                "        if (test.size() != success.size()) {\n" +
                "            // 个数不同\n" +
                "            return \"实际输出：\" + test + \"\\n\";\n" +
                "        }\n" +
                "        for (int i = 0; i < success.size(); i++) {\n" +
                "            if (test.get(i).start != success.get(i).start\n" +
                "                    || test.get(i).end != success.get(i).end) {\n" +
                "                return \"实际输出：\" + test + \"\\n\";\n" +
                "            }\n" +
                "        }\n" +
                "        return \"success\";\n" +
                "    }\n" +
                "    public static void main(String[] args) {\n" +
                "        Main test = new Main();\n" +
                "        // test1\n" +
                "        ArrayList<Interval> tests1 = new ArrayList<>();\n" +
                "        tests1.add(new Interval(10,12));\n" +
                "        tests1.add(new Interval(10,20));\n" +
                "        tests1.add(new Interval(19,31));\n" +
                "        tests1.add(new Interval(32,39));\n" +
                "        tests1.add(new Interval(38,90));\n" +
                "        tests1.add(new Interval(100,101));\n" +
                "        ArrayList<Interval> re1 = test.merge(tests1);\n" +
                "        // [10,31] [32,90] [100,101]\n" +
                "        ArrayList<Interval> success1 = new ArrayList<>();\n" +
                "        success1.add(new Interval(10,31));\n" +
                "        success1.add(new Interval(32,90));\n" +
                "        success1.add(new Interval(100,101));\n" +
                "        if (!isTrue(re1,success1).equals(\"success\")) {\n" +
                "            System.out.println(\"test：\" + tests1 + \"\\n\" + isTrue(re1,success1) + \"success:\" + success1);\n" +
                "            return;\n" +
                "        } else {\n" +
                "            System.out.println(\"\\ntest1 pass\");\n" +
                "        }\n" +
                "        // test2\n" +
                "        ArrayList<Interval> tests2 = new ArrayList<>();\n" +
                "        ArrayList<Interval> re2 = test.merge(tests2);\n" +
                "        // []\n" +
                "        ArrayList<Interval> success2 = new ArrayList<>();\n" +
                "        if (!isTrue(re2,success2).equals(\"success\")) {\n" +
                "            System.out.println(\"test：\" + tests2+ \"\\n\" + isTrue(re2,success2)+ \"success:\" + success2);\n" +
                "            return;\n" +
                "        }else {\n" +
                "            System.out.println(\"\\ntest2 pass\");\n" +
                "        }\n" +
                "        // test3\n" +
                "        ArrayList<Interval> tests3 = new ArrayList<>();\n" +
                "        tests3.add(new Interval(0,1));\n" +
                "        ArrayList<Interval> re3 = test.merge(tests3);\n" +
                "        // [0,1]\n" +
                "        ArrayList<Interval> success3 = new ArrayList<>();\n" +
                "        success3.add(new Interval(0,1));\n" +
                "        if (!isTrue(re3,success3).equals(\"success\")) {\n" +
                "            System.out.println(\"test：\" + tests3+ \"\\n\" + isTrue(re3,success3)+ \"success:\" + success3);\n" +
                "            return;\n" +
                "        } else {\n" +
                "            System.out.println(\"\\ntest3 pass\");\n" +
                "        }\n" +
                "        // test4\n" +
                "        ArrayList<Interval> tests4 = new ArrayList<>();\n" +
                "        tests4.add(new Interval(10,20));\n" +
                "        tests4.add(new Interval(20,40));\n" +
                "        ArrayList<Interval> re4 = test.merge(tests4);\n" +
                "        // [10,40]\n" +
                "        ArrayList<Interval> success4 = new ArrayList<>();\n" +
                "        success3.add(new Interval(10,40));\n" +
                "        if (!isTrue(re3,success3).equals(\"success\")) {\n" +
                "            System.out.println(\"test：\" + tests3+ \"\\n\" + isTrue(re4,success4)+ \"success:\" + success4);\n" +
                "            return;\n" +
                "        } else {\n" +
                "            System.out.println(\"\ntest4 pass\");\n" +
                "        }\n" +
                "    }");
        ojMapper.addProblem(oj);
    }

    @Test
    @Transactional
    void delProblem() {
        System.out.println(ojMapper.delProblem(1));
    }


    @Test
    void queryById() {
        System.out.println(ojMapper.queryById(2));
    }

    @Test
    void queryAll() {
        System.out.println(ojMapper.queryAll(0,10));
    }
}