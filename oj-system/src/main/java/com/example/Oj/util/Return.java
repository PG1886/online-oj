package com.example.Oj.util;

import lombok.Data;

@Data
public class Return {
    private Integer code;
    private String message;
    private Object data;

    private Return(Integer code,String message,Object data) {
        this.code = code;
        this.message = message;
        this.data = data;
    }

    public static Return success(Object data) {
        return new Return(Code.SUCCESS_NORMAL.getCode(), Code.SUCCESS_NORMAL.getMessage(),data);
    }

    public static Return success(Code code) {
        return new Return(code.getCode(),code.getMessage(),null);
    }

    public static Return success(Code code, Object data) {
        return new Return(code.getCode(),code.getMessage(),data);
    }

    public static Return fail(Code code) {
        return new Return(code.getCode(),code.getMessage(),null);
    }

    public static Return fail(Code code,Object data) {
        return new Return(code.getCode(), code.getMessage(),data);
    }
}
