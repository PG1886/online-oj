package com.example.Oj.util;

import org.springframework.util.DigestUtils;
import org.springframework.util.StringUtils;

import java.util.UUID;

public class Md5WithSalt {
    public static String encrypt(String inputPassword) {
        // 1. 生成盐值
        String salt = UUID.randomUUID().toString().replace("-","");

        // 2. 拼接后md5
        String middle = DigestUtils.md5DigestAsHex((salt + inputPassword).getBytes());

        // 3. 最终密文
        return salt + "^" + middle;
    }

    public static boolean check(String inputPassword,String dbPassword) {
        // 1. 参数校验
        if (!StringUtils.hasLength(dbPassword)
                || !StringUtils.hasLength(inputPassword)
                || dbPassword.length() != 65) {
            return false;
        }

        // 2. 根据数据库密码中盐值生成输入密码的密文
        String salt = dbPassword.split("\\^")[0];
        String inputFinalPassword = encrypt(salt,inputPassword);

        // 3. 密文与数据库中密文对比
        return dbPassword.equals(inputFinalPassword);
    }

    private static String encrypt(String salt, String password) {
        return salt + "^" + DigestUtils.md5DigestAsHex((salt + password).getBytes());
    }
}
