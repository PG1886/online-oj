package com.example.Oj.util;

public enum Code {
    EXCEPTION(500,"exception"),
    SUCCESS_NORMAL(200,"success");

    private Integer code;
    private String message;

    Code(Integer code, String message) {
        this.code = code;
        this.message = message;
    }

    public Integer getCode() {
        return code;
    }

    public String getMessage() {
        return message;
    }
}
