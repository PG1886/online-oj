package com.example.Oj.interceptor;

import com.example.Oj.util.Code;
import com.example.Oj.util.Return;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

@ControllerAdvice
public class HandlerException {
    @ExceptionHandler(Exception.class)
    public Return exceptionHandler(Exception e) {
        return Return.fail(Code.EXCEPTION);
    }
}
